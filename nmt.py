# -*- coding: utf-8 -*-
#@title Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# import required libraries
import tensorflow as tf
import unicodedata
import re
import numpy as np
import os
import io
import json
import time
import argparse
from sklearn.model_selection import train_test_split


##################################################
## data and string preparation and manipulation ##
##################################################


# converts the unicode string to ascii
# argument: sentence/string
def unicode_to_ascii(s):
  return ''.join(c for c in unicodedata.normalize('NFD', s)
    if unicodedata.category(c) != 'Mn')


# argument: sentence/string
def preprocess_sentence(w):
  # make lower case, strip leading and trailing whitespace, convert to ascii
  w = unicode_to_ascii(w.lower().strip())
  
  # adding a <start> and an <end> token to the sentence so that the model knows when to start and stop predicting.
  w = '<start> ' + w + ' <end>'
  
  # replace consecutive whitespaces by one whitespace
  w = re.sub(r'[" "]+', " ", w)
  
  # strip whitespace
  w = w.rstrip().strip()
  return w


# arguments:  array of sentences
#             minimum frequency for words to be kept in dictionary
def tokenize(lang, min_freq):
  # initialize Tokenizer; filter='' means we do not filter any characters/tokens
  lang_tokenizer = tf.keras.preprocessing.text.Tokenizer(filters='')
  # fit tokenizer on list of sentences in lang
  lang_tokenizer.fit_on_texts(lang)
  
  # create list with words occuring min_freq or more many times
  high_count_words = [w for w,c in lang_tokenizer.word_counts.items() if c >= min_freq]
  # create dictionary mapping frequent words to numbers, 0 is reserved for padding and 1 for unknown words
  word_index = { high_count_words[i] : i + 2 for i in range(len(high_count_words)) }
  # assign 1 to special unknown token
  word_index['<UNK>'] = 1
  # create reverse index for number -> string lookup
  index_word = { v : k for k,v in word_index.items() }
  
  # convert each sentence in lang to sequence of integers representing the tokens from the sentence according to tokenizer; unknown words (not in dictionary) are mapped to 1
  tensor = [[word_index.get(word, 1) for word in sentence.split(' ')] for sentence in lang]
  
  # pads each sequence in tensor with 0's to the length of the longest sequence in tensor and returns this as numpy array; padding is added at the end of every sequence
  tensor = tf.keras.preprocessing.sequence.pad_sequences(tensor, padding='post')
  
  # create new LanguageDictionary with created lookup tables
  lang_dict = LanguageDictionary(word_index, index_word)
  
  # return tokenized sequences and the dictionary
  return tensor, lang_dict


# arguments:  input_path: path to input sentence file as string
#             target_path: path to target sentence file as string
#             num_examples: maximum number of sentence pairs to load
#             min_freq: all words in the loaded sentences which occur less than min_freq times are replaced by '<UNK>'
#             max_length: sentences containing more tokens than max_length are not loaded (0: no restriction)
def load_dataset(input_path, target_path, num_examples=None, min_freq=0, max_length=0):
  print("Reading sentences ...")
  
  # load input and target sentences
  input_lines = io.open(input_path, encoding='UTF-8').read().strip().split('\n')
  target_lines = io.open(target_path, encoding='UTF-8').read().strip().split('\n')
  
  print("Preprocessing sentences ...")
  
  # preprocess all sentences if max_length > 0, otherwise only num_examples many
  if max_length > 0:
    num_prep = None
  else:
    num_prep = num_examples
  
  # preprocess sentences
  input_lines = [preprocess_sentence(s) for s in input_lines[:num_prep]]
  target_lines = [preprocess_sentence(s) for s in target_lines[:num_prep]]
  
  # if a maximum sentence length is enforced, discard long sentences
  if max_length > 0:
    print("Filtering sentences ...")
    
    def first_n(iterable):
      if num_examples:
        c = 0
        for inp,tar in iterable:
          if c < num_examples and len(inp.split(' ')) < max_length:
            c += 1
            yield inp,tar
      else:
        for inp,tar in iterable:
          if len(inp.split(' ')) < max_length:
            yield inp,tar
    
    # create list with short sentence pairs
    short = list(first_n(zip(input_lines, target_lines)))
    # unzip pairs
    input_lines, target_lines = zip(*short)
  
  print("Loaded {} sentence pairs".format(len(input_lines)))
  
  # creating training and validation sets using an 80-20 split but at most 9984 sentence pairs (divisible by 128)
  test_size = 9984 if len(input_lines) * 0.2 > 9984 else 0.2
  input_lines_train, input_lines_val, target_lines_train, target_lines_val = train_test_split(input_lines, target_lines, test_size=test_size, shuffle=True)
  print("Using {} sentence pairs for training and {} for validation".format(len(input_lines_train), len(input_lines_val)))
  
  # tokenize sentences
  input_tensor_train, inp_dict = tokenize(input_lines_train, min_freq)
  target_tensor_train, targ_dict = tokenize(target_lines_train, min_freq)
  
  # convert each sentences to sequences of integers
  input_tensor_val = [[inp_dict.word_index.get(word, 1) for word in sentence.split(' ')] for sentence in input_lines_val]
  target_tensor_val = [[targ_dict.word_index.get(word, 1) for word in sentence.split(' ')] for sentence in target_lines_val]

  # pad sequences
  input_tensor_val = tf.keras.preprocessing.sequence.pad_sequences(input_tensor_val, padding='post')
  target_tensor_val = tf.keras.preprocessing.sequence.pad_sequences(target_tensor_val, padding='post')
  
  return input_tensor_train, target_tensor_train, input_tensor_val, target_tensor_val, inp_dict, targ_dict


# custom dictionary class including functions to save and load dictionaries
class LanguageDictionary:
  def __init__(self, word_index=None, index_word=None):
    self.word_index = word_index
    self.index_word = index_word
  
  
  def save_to_file(self, filename):
    with open(filename, 'w') as json_file:
      json.dump(self.word_index, json_file)
  
  
  def load_from_file(self, filename):
    with open(filename, 'r') as json_file:
      self.word_index = json.load(json_file)
      self.index_word = { v : k for k,v in self.word_index.items() }


######################
## neural net model ##
######################


# Encoder model to turn sentences into thought vectors; inherits from class tf.keras.Model
class Encoder(tf.keras.Model):
  # constructor
  # arguments:  pointer to itself (python standard),
  #             number of different tokens in input language,
  #             dimension of input language word vectors (embedding layer),
  #             dimension of encoder thought vectors (hidden layer),
  #             batch size for batch gradient descent
  def __init__(self, vocab_size, enc_embedding_dim, enc_hidden_dim, dropout=0.0):
    #call constructor of parent class
    super(Encoder, self).__init__()
    
    #-=-=-=-=-=-=-=-=-=-=-#
    # TODO: create layers #
    #-=-=-=-=-=-=-=-=-=-=-#
  
  
  # define call method, invoked when an instance 'encoder' of class Encoder is called by encoder(...)
  # arguments:  pointer to itself,
  #             batch of sequences of input token indices to process, x.shape = (batch_size, max_length_inp)
  #             initial hidden state for each sequence in batch, initial_hidden.shape = (batch_size, enc_hidden_bi_dim)
  #             flag whether we are in training or inference mode (only relevant for dropout)
  def call(self, x, initial_hidden, training=False):
    #-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=#
    # TODO: run input through layers #
    #-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=#
  
  
  # create initial state(s) for encoder
  def initialize_hidden_state(self, batch_size):
    #-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=#
    # TODO: create initial states for layer(s) #
    #-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=#


# BahdanauAttention model to compute context vectors and attention weights from encoder thought vectors and a decoder thought vector
# inherits from class tf.keras.Model
class BahdanauAttention(tf.keras.Model):
  # define constructor
  # arguments:  pointer to self
  #             dimension of scoring matrix output vectors
  def __init__(self, scoring_matrix_dim):
    super(BahdanauAttention, self).__init__()
    # create new densely connected layers with linear (default) activation function, i.e., matrices
    self.W_enc = tf.keras.layers.Dense(scoring_matrix_dim)
    self.W_dec = tf.keras.layers.Dense(scoring_matrix_dim)
    self.V = tf.keras.layers.Dense(1)
  
  
  # define call method, invoked when an instance 'attention' of class BahdanauAttention is called by attention(...)
  # arguments:  pointer to self,
  #             batch of decoder thought vectors, dec_hidden.shape = (batch_size, dec_hidden_dim)
  #             batch of sequences of encoder thought vectors, enc_hidden.shape = (batch_size, max_length_inp, enc_hidden_dim)
  #             flag whether we are in training or inference mode (only relevant for dropout)
  def call(self, dec_hidden, enc_hidden):
    # expand dimension of dec_hidden along axis 1
    # needed for addition with enc_hidden by numpy 'broadcasting'
    # decoder_hidden_with_time_axis.shape = (batch_size, 1, dec_hidden_dim)
    decoder_hidden_with_time_axis = tf.expand_dims(dec_hidden, 1)

    # compute attention scores pairwise for each encoder and decoder thought vector
    # self.W1(enc_hidden).shape = (batch_size, max_length_inp, scoring_matrix_dim)
    # self.W2(decoder_hidden_with_time_axis).shape = (batch_size, 1, scoring_matrix_dim)
    # sum of both has shape (batch_size, max_length_inp, scoring_matrix_dim) through broadcasting
    # tanh is applied pointwise and does not change shape
    # score.shape = (batch_size, max_length_inp, 1), i.e., for every batch and every thought vector of the encoder, we have one score with the decoder thought vector
    score = self.V(tf.nn.tanh(self.W_enc(enc_hidden) + self.W_dec(decoder_hidden_with_time_axis)))

    # calculate softmax along axis one, i.e., for every batch caculate softmax over the scores of the sequence of this batch
    # attention_weights.shape = (batch_size, max_length_inp, 1)
    attention_weights = tf.nn.softmax(score, axis=1)

    # multiply every encoder thought vector with its score (by broadcasting)
    # context_vector.shape = (batch_size, max_length_inp, enc_hidden_dim)
    context_vector = attention_weights * enc_hidden
    
    # sum tensor along axis one, thereby reducing dimension,
    # i.e., context_vector[x,z] = context_vector[x,1,z] + ... + context_vector[x,max_length_inp,z]
    # context_vector.shape = (batch_size, enc_hidden_dim)
    context_vector = tf.reduce_sum(context_vector, axis=1)
    
    return context_vector, attention_weights


# Decoder model to predict a target token from the encoder thought vectors, the current decoder thought vector, and the previous predicted target token
class Decoder(tf.keras.Model):
  # constructor
  # arguments:  pointer to itself,
  #             number of different tokens in target language,
  #             dimension of target language word vectors (embedding layer),
  #             dimension of decoder thought vectors (hidden layer),
  #             batch size for batch gradient descent
  def __init__(self, vocab_size, dec_embedding_dim, dec_hidden_dim, dropout=0.0):
    #call constructor of parent class
    super(Decoder, self).__init__()
    
    # intialize all internal values
    self.dec_hidden_dim = dec_hidden_dim
    
    # create new embedding layer to transform target language token indices to word vectors
    # essentially a matrix of dimension vocab_size * dec_embedding_dim where the input is one-hot encoded to select the embedding vector
    self.embedding = tf.keras.layers.Embedding(vocab_size, dec_embedding_dim)
    
    # create new GRUs (gated recurrent unit) layer of specified dimension
    # return_state: after list of thought vectors, return last thought vector separately once more
    self.gru1 = tf.keras.layers.GRU(self.dec_hidden_dim,
                                    return_sequences=True,
                                    return_state=True,
                                    recurrent_initializer='glorot_uniform',
                                    dropout=dropout)
    self.gru2 = tf.keras.layers.GRU(self.dec_hidden_dim,
                                    return_sequences=True,
                                    return_state=True,
                                    recurrent_initializer='glorot_uniform',
                                    dropout=dropout)
    
    # create fully connected (dense) layer for target token predictions
    self.fully_connected = tf.keras.layers.Dense(vocab_size)

    # attention model
    self.attention = BahdanauAttention(self.dec_hidden_dim)
  
  
  # define call method, invoked when an instance 'decoder' of class Decoder is called by decoder(...)
  # arguments:  pointer to itself,
  #             batch of target language token indices (last ouput words), x.shape = (batch_size, 1)
  #             batch of decoder thought vectors, dec_hidden.shape = (batch_size, dec_hidden_dim)
  #             batch of sequences of encoder thought vectors, enc_hidden.shape = (batch_size, max_length_inp, enc_hidden_dim)
  def call(self, x, dec_hidden1, dec_hidden2, enc_hidden, training=False):
    # transform batch target language token indices x to batch of word vectors using the embedding layer
    # x.shape = (batch_size, 1, dec_embedding_dim)
    x = self.embedding(x)
    
    # compute context vector and attention weights from current decoder thought vector and encoder thought vectors
    # context_vector.shape = (batch_size, enc_hidden_dim)
    # attention_weights.shape = (batch_size, max_length_inp, 1)
    context_vector, attention_weights = self.attention(dec_hidden1, enc_hidden)
    
    # expand dimension of context vector to add to inputs
    context_vector = tf.expand_dims(context_vector, 1)
    
    # enrich input by context vector
    x = tf.concat([context_vector,x],axis=-1)
    
    # run batch of concatenated vectors x through GRU, save hidden state into output, save last state of each batch to state (for GRUs, output and state coincide)
    # output shape: output1.shape = (batch_size, 1, dec_hidden_dim)
    #               state1.shape = (batch_size, dec_hidden_dim)
    output1, state1 = self.gru1(x, initial_state=dec_hidden1, training=training)
    
    # enrich output1 by context_vector
    output1 = tf.concat([context_vector,output1],axis=-1)
    
    # run through second layer
    output2, state2 = self.gru2(output1, initial_state=dec_hidden2, training=training)
    
    # reshape output tensor to remove axis 1
    # output2.shape = (batch_size, dec_hidden_dim)
    output2 = tf.reshape(output2, (-1, output2.shape[2]))
    
    # pass batch of new decoder thought vectors through fully_connected to create target token predictions, i.e., some number for each of the vocab_size many target language tokens
    # x.shape = (batch_size, vocab_size)
    x = self.fully_connected(output2)
    
    return x, state1, state2, attention_weights
    
    
  # create tensor of shape (batch_size, dec_hidden_dim) containing all zeros
  def initialize_hidden_state(self, batch_size):
    return tf.zeros((batch_size, self.dec_hidden_dim))


# create new loss object to compute categorical cross entropy of target token predictions versus the actually correct word
# from_logits: treat predictions as logits (logical units), not as probability distribution
# reduction = 'none': return batch of losses, in contrast to 'sum' (sum of losses) and 'sum_over_batch_size' (mean)
crossentropy = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True, reduction='none')

# loss function to compute loss of a whole batch using categorical cross entropy
# arguments: batch of correct next target token indices, real.shape = (batch_size)
#            batch of next target token predictions (one 'logit' for each possible target token), pred.shape = (batch_size, vocab_size)
def loss_function(real, pred):
  # create batch of truth values: False if target token is 0 (padding), True otherwise
  # mask.shape = (batch_size)
  mask = tf.math.logical_not(tf.math.equal(real, 0))
  
  #loss_.shape = (batch_size)
  loss_ = crossentropy(real, pred)

  # cast mask to same data type as loss_; True becomes 1 and False becomes 0
  mask = tf.cast(mask, dtype=loss_.dtype)
  # zero out losses that tried to predict a padding 0
  loss_ *= mask

  # calculate and return mean batch loss (now does not include losses for padding predictions anymore)
  return tf.reduce_mean(loss_)


######################
## translator class ##
######################


class Translator:
  # batch size for batch gradient descent (smoother than stochastic gradient descent, faster than full gradient descent)
  def train_from_corpus(self, input_path, target_path, model_dir, epochs=10, batch_size=64, min_freq=0, num_examples=None, max_length=0, dropout=0.0, contin=False):
    # store dropout rate
    self.dropout = dropout
    
    # load and tokenize sentences from dataset
    input_tensor_train, target_tensor_train, input_tensor_val, target_tensor_val, self.inp_dict, self.targ_dict = load_dataset(input_path=input_path, target_path=target_path, num_examples=num_examples, min_freq=min_freq, max_length=max_length)
    
    # calculate max_length of the target tensors (i.e. length of longest sentence)
    self.max_length_inp, self.max_length_targ = max(input_tensor_train.shape[1], input_tensor_val.shape[1]), max(target_tensor_train.shape[1], target_tensor_val.shape[1])
    
    # dimension of our embedding layer, i.e., size of our word vectors
    self.embedding_dim = 128
    # dimension of our encoder and decoder hidden states, i.e., dimension of our thought vectors
    self.coder_hidden_dim = 128
    # number of different tokens for each of our languages, i.e., size of our dictionaries (+1 is for the padding)
    vocab_inp_size = len(self.inp_dict.word_index) + 1
    vocab_tar_size = len(self.targ_dict.word_index) + 1
    
    # save parameters and dictionaries in case we want to interrupt training early
    self.save_to_dir(model_dir, save_weights=False)
    
    # number of sentence pairs for training
    train_size = len(input_tensor_train)
    # number of batches we have to process in every epoch
    train_per_epoch = train_size//batch_size
    # create dataset of pairs (input, target) and shuffle (buffer size for sliding window shuffle = train_size)
    trainset = tf.data.Dataset.from_tensor_slices((input_tensor_train, target_tensor_train)).shuffle(train_size)
    # split dataset into batches of size batch_size, drop last batch if it has less elements than batch_size
    trainset = trainset.batch(batch_size, drop_remainder=True)
    
    # same for validation set
    val_size = len(input_tensor_val)
    val_num_batches = val_size//batch_size
    valset = tf.data.Dataset.from_tensor_slices((input_tensor_val, target_tensor_val)).shuffle(val_size)
    valset = valset.batch(batch_size, drop_remainder=True)
    
    # intitialize encoder and decoder
    self.encoder = Encoder(vocab_inp_size, self.embedding_dim, self.coder_hidden_dim, dropout=self.dropout)
    self.decoder = Decoder(vocab_tar_size, self.embedding_dim, self.coder_hidden_dim, dropout=self.dropout)
    # intitialize gradient descent algorithm (here: Adam -- adaptive moment estimation)
    self.optimizer = tf.keras.optimizers.Adam()
    
    #initialize checkpoint for saving and loading training progress
    checkpoint_dir = os.path.join(model_dir, 'training_checkpoints')
    checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt")
    checkpoint = tf.train.Checkpoint(optimizer=self.optimizer, encoder=self.encoder, decoder=self.decoder)
    
    # in case we interrupted training and want to continue from last checkpoint, restore it
    if contin:
      checkpoint.restore(tf.train.latest_checkpoint(checkpoint_dir))
    
    # helper function for validation
    def validate(amount=val_num_batches):
      val_loss = 0
      for inp, targ in valset.take(amount):
        val_loss += self._score(inp, targ, initial_enc_hidden, batch_size)
      return val_loss / amount
    
    # train model with corpus for specified number of epochs
    for epoch in range(epochs):
      start = time.time()

      # intial_enc_hidden.shape = (1, enc_hidden_dim)
      initial_enc_hidden = self.encoder.initialize_hidden_state(batch_size)
      total_loss = 0

      # loop over enumerated batches and train model with each batch
      # inp.shape = (batch_size, max_length_inp)
      # targ.shape = (batch_size, max_length_targ)
      for (batch, (inp, targ)) in enumerate(trainset.take(train_per_epoch)):
        batch_loss = self.train_step(inp, targ, initial_enc_hidden, batch_size)
        total_loss += batch_loss

        # print progress every 100 batches
        if batch % 100 == 0:
          print('Epoch {} Batch {} Loss {:.4f}'.format(epoch + 1, batch, batch_loss.numpy()))
        
        # every 2000 batches, calculate loss on validation set
        if batch % 2000 == 0 and batch >= 2000 and (train_per_epoch - batch) > 2000:
          print('Epoch {} Batch {} Validation Loss {:.4f}'.format(epoch + 1, batch, validate()))
      
      # save (checkpoint) the model every epoch
      checkpoint.save(file_prefix = checkpoint_prefix)
      # touch all internal weights for saving
      self.evaluate('')
      # save encoder and decoder weights
      self.encoder.save_weights(os.path.join(checkpoint_dir, 'encoder_weights_epoch{:02}.h5'.format(epoch+1)))
      self.decoder.save_weights(os.path.join(checkpoint_dir, 'decoder_weights_epoch{:02}.h5'.format(epoch+1)))
      # calculate loss on validation set
      val_loss = validate()

      # report losses and time taken
      print('Epoch {} Loss {:.4f}'.format(epoch + 1, total_loss / train_per_epoch))
      print('Epoch {} Validation Loss {:.4f}'.format(epoch + 1, val_loss))
      print('Time taken for 1 epoch {} sec'.format(time.time() - start))
      print(time.strftime("Time: %a, %b %d %H:%M:%S\n"))
    
    # save model after training
    self.save_to_dir(model_dir)
  
  
  # translate inp and score translation
  # see train_step for more information
  @tf.function
  def _score(self, inp, targ, initial_enc_hidden, batch_size):
    loss = 0
    enc_output, enc_state = self.encoder(inp, initial_enc_hidden)
    dec_hidden1 = enc_state
    dec_hidden2 = enc_state
    dec_input = [self.targ_dict.word_index['<start>']] * batch_size
    dec_input = tf.expand_dims(dec_input, 1)
    
    for tar_index in range(1, targ.shape[1]):
      predictions, dec_hidden1, dec_hidden2, _ = self.decoder(dec_input, dec_hidden1, dec_hidden2, enc_output)
      loss += loss_function(targ[:, tar_index], predictions)
      predictions = tf.argmax(predictions, axis=-1)
      dec_input = tf.expand_dims(predictions, 1)
    
    return (loss / int(targ.shape[1]))
  
  
  # define training step, i.e., how each batch modifies network
  # @tf.function disables 'eager execution' (increases training speed)
  # arguments:  batch of sequences of input token indices; inp.shape = (batch_size, max_length_inp)
  #             batch of sequences of target token indices (translations); targ.shape = (batch_size, max_length_targ)
  #             batch of initial encoder hidden states; initial_enc_hidden.shape = (batch_size, enc_hidden_dim)
  @tf.function
  def train_step(self, inp, targ, initial_enc_hidden, batch_size):
    # initialize sum of losses to zero
    loss = 0
    
    # context manager block with a GradientTape as the context manager
    # used to compute gradient of our layers with respect to losses
    # GradientTape tape watches every trainable variable accessed inside its context
    with tf.GradientTape() as tape:
      # enc_output.shape = (batch_size, max_length_inp, enc_hidden_dim)
      enc_output, enc_state = self.encoder(inp, initial_enc_hidden, training=True)
      
      # create initial hidden states for decoder
      dec_hidden1 = enc_state
      dec_hidden2 = enc_state
      
      # create array containing target token index for <start> batch_size many times
      dec_input = [self.targ_dict.word_index['<start>']] * batch_size
      # expand dimension along axis 1; dec_input.shape = (batch_size, 1)
      dec_input = tf.expand_dims(dec_input, 1)
      
      ##### Teacher forcing -- feeding the target as the next input
      # loop over target sequences
      # start with 1 as token at 0 is always <start>, which we do not predict
      # range(x,y) includes x but excludes y
      for tar_index in range(1, targ.shape[1]):
        # use decoder to make predictions for next target token using previous decoder prediction, previous decoder thought vector and encoder thought vectors generated from input sequence
        # in the first step, the previous decoder prediction is simply <start>, the previous decoder thought vector is the encoder's last thought vector
        # for training, we use 'teacher forcing', i.e., we do not use the previous decoder prediction, but the correct word which should have been predicted instead
        predictions, dec_hidden1, dec_hidden2, _ = self.decoder(dec_input, dec_hidden1, dec_hidden2, enc_output, training=True)
        
        # calculate loss for batch and add to loss
        # targ[:,tar_index] takes for each batch the token index at tar_index; resulting shape is (batch_size, 1)
        loss += loss_function(targ[:, tar_index], predictions)
        
        # teacher forcing
        dec_input = tf.expand_dims(targ[:, tar_index], 1)
    
    # calculate mean batch loss by dividing accumulated loss by the target sequence length
    batch_loss = (loss / int(targ.shape[1]))
    
    # create array of trainable variables
    variables = self.encoder.trainable_variables + self.decoder.trainable_variables
    
    # calculate gradient of trainable variables with respect to loss
    gradient = tape.gradient(loss, variables)
    
    # use optimizer (Adam) to apply backpropagation to trainable variables using gradients
    self.optimizer.apply_gradients(zip(gradient, variables))
    
    return batch_loss
  
  
  # helper function to store model folder model_dir
  def save_to_dir(self, model_dir, save_weights=True):
    # create directory if it does not exist
    if not os.path.exists(model_dir):
      os.makedirs(model_dir)
    
    # create python dictionary with all parameters needed to restore the model
    params = {'embedding_dim' : self.embedding_dim,
              'coder_hidden_dim' : self.coder_hidden_dim,
              'dropout' : self.dropout}
    
    # converts params to json and save as params.json
    with open(os.path.join(model_dir, 'params.json'), 'w') as json_file:
      json.dump(params, json_file)
      
    if save_weights:
      # touch all internal weights for saving
      self.evaluate('a')
      
      self.encoder.save_weights(os.path.join(model_dir, 'encoder_weights.h5'))
      self.decoder.save_weights(os.path.join(model_dir, 'decoder_weights.h5'))
    
    # save dictionaries to disc
    self.inp_dict.save_to_file(os.path.join(model_dir, 'inp_dict.json'))
    self.targ_dict.save_to_file(os.path.join(model_dir, 'targ_dict.json'))
  
  
  # helper function to load trained model from folder model_dir
  # if epoch ist specified, loads the respective epoch from the checkpoint directory
  def load_from_dir(self, model_dir, epoch=None):
    if epoch:
      print("Loading epoch {} from directory '{}'".format(epoch, model_dir))
    else:
      print("Loading model from directory '{}'".format(args.model_dir))
    
    self.max_length_inp = 80
    self.max_length_targ = 80
    
    # create input and target dictionaries and load them from disc
    self.inp_dict = LanguageDictionary()
    self.inp_dict.load_from_file(os.path.join(model_dir, 'inp_dict.json'))
    self.targ_dict = LanguageDictionary()
    self.targ_dict.load_from_file(os.path.join(model_dir, 'targ_dict.json'))

    vocab_inp_size = len(self.inp_dict.word_index) + 1
    vocab_tar_size = len(self.targ_dict.word_index) + 1
    
    # load parameters
    with open(os.path.join(model_dir, 'params.json'), 'r') as json_file:
      params = json.load(json_file)
    
    # create Encoder and Decoder from parameters
    self.encoder = Encoder(vocab_inp_size, params['embedding_dim'], params['coder_hidden_dim'], dropout=params['dropout'])
    self.decoder = Decoder(vocab_tar_size, params['embedding_dim'], params['coder_hidden_dim'], dropout=params['dropout'])
    
    # touch all layers once to initialize variables for loading
    self.evaluate('')
  
    if epoch:
      self.encoder.load_weights(os.path.join(model_dir, 'training_checkpoints', 'encoder_weights_epoch{:02}.h5'.format(epoch)))
      self.decoder.load_weights(os.path.join(model_dir, 'training_checkpoints', 'decoder_weights_epoch{:02}.h5'.format(epoch)))
    else:
      self.encoder.load_weights(os.path.join(model_dir, 'encoder_weights.h5'))
      self.decoder.load_weights(os.path.join(model_dir, 'decoder_weights.h5'))
  
  
  # translate single sentence (string)
  def evaluate(self, sentence):
    sentence = preprocess_sentence(sentence)

    # split sentence at ' ' into tokens, look up token indices for these tokens, and store them into an array
    split = sentence.split(' ')
    sequence = [self.inp_dict.word_index.get(i, 1) for i in split]
    # pad token sequence to max_length_inp
    tensor = tf.keras.preprocessing.sequence.pad_sequences([sequence], maxlen=self.max_length_inp, padding='post')
    tensor = tf.convert_to_tensor(tensor)
    
    # intitialize translation to empty string
    result = ''
    
    # create initial hidden encoder state
    # intial_enc_hidden.shape = (1, enc_hidden_dim)
    initial_enc_hidden = self.encoder.initialize_hidden_state(1)
    
    # pass input sentence through encoder
    # enc_output.shape = (1, max_length_inp, enc_hidden_dim)
    enc_output, enc_state = self.encoder(tensor, initial_enc_hidden)
    
    # create initial hidden states for decoder
    dec_hidden1 = enc_state
    dec_hidden2 = enc_state
    # create tensor of shape (1,1) containing target token index for <start>
    dec_input = tf.expand_dims([self.targ_dict.word_index['<start>']], 0)
    
    for tar_index in range(self.max_length_targ):
      # create predictions for next target token
      # predictions.shape = (1, vocab_tar_size)
      predictions, dec_hidden1, dec_hidden2, attention_weights = self.decoder(dec_input, dec_hidden1, dec_hidden2, enc_output)
      
      # get token index with highest probability
      predicted_id = tf.argmax(predictions[0]).numpy()
      
      # stop translating after predicting the sentence end marker <end>
      if self.targ_dict.index_word[predicted_id] == '<end>':
        return result, sentence
      
      # look up token for token index and append it to target sentence
      # if we predicted '<UNK>', i.e. 1, copy the input word with the highest attention
      # if our maximum attention is on an unknown input word, copy the unknown word to the output
      attention_weights = tf.reshape(attention_weights, [-1]).numpy()[:len(split)]
      max_attention = np.argmax(attention_weights)
      
      if predicted_id == 1 and len(split) > 2:
        while split[max_attention] == '<start>' or split[max_attention] == '<end>':
          attention_weights[max_attention] = 0
          max_attention = np.argmax(attention_weights)
          
        if (1 in sequence):
          while sequence[max_attention] != 1:
            attention_weights[max_attention] = 0
            max_attention = np.argmax(attention_weights)
            
        result += split[max_attention] + ' '
      else:
        result += self.targ_dict.index_word[predicted_id] + ' '
      
      # create tensor of shape (1, 1) containing predicted token index to feed it back into the decoder for the next prediction
      dec_input = tf.expand_dims([predicted_id], 0)
    
    return result, sentence
  
  
  # argument: string
  def translate(self, sentence):
    # evaluate sentence to obtain translation and preprocessed input sentence
    result, sentence = self.evaluate(sentence)
    
    print('Input: %s' % (sentence))
    print('Predicted translation: {}'.format(result))
  
  
  # start interactive translation mode
  def interactive(self):
    sentence = input("Interactive translation mode\n")
    while "" != sentence:
      self.translate(sentence)
      sentence = input()
  
  
  # translate from file to file with specified batch_size (faster)
  def translate_to_file(self, inp_file, output_file, batch_size=128):
    print("Translating '{}' to '{}' in batches of {}".format(inp_file, output_file, batch_size))
    
    # load lines to translate
    input_lines = io.open(inp_file, encoding='UTF-8').read().strip().split('\n')
    
    pad_amount = batch_size - (len(input_lines) % batch_size)
    
    # add empty strings to make number of lines divisible by batch_size
    input_lines += ([''] * pad_amount)
    
    number_of_batches = len(input_lines) // batch_size
    
    batch_splits = [preprocess_sentence(sentence).split(' ') for sentence in input_lines]
    batch_sequences = [[self.inp_dict.word_index.get(word, 1) for word in split] for split in batch_splits]
    batch_tensor = tf.keras.preprocessing.sequence.pad_sequences(batch_sequences, maxlen=self.max_length_inp, padding='post')
    
    batchset = tf.data.Dataset.from_tensor_slices(batch_tensor)
    batchset = batchset.batch(batch_size)
    
    end_index = self.targ_dict.word_index['<end>']
    
    with open(output_file, 'w') as output_file:
      for batch_index, batch_tensor in enumerate(batchset.take(number_of_batches)):
        offset = batch_index * batch_size
        
        enc_output, enc_state = self.encoder(batch_tensor, self.encoder.initialize_hidden_state(batch_size))
        dec_hidden1 = enc_state
        dec_hidden2 = enc_state
        dec_input = [self.targ_dict.word_index['<start>']] * batch_size
        dec_input = tf.expand_dims(dec_input, 1)
        # mask for which translations we already predicted <end>, i.e., we don't want further output (list of length batch_size)
        prediction_complete_mask = [False] * batch_size
        # array of empty translations (list of length batch_size)
        translations = [''] * batch_size
        
        for tar_index in range(self.max_length_targ):
          predictions, dec_hidden1, dec_hidden2, batch_attention_weights = self.decoder(dec_input, dec_hidden1, dec_hidden2, enc_output)
          # predicted_ids.shape = (batch_size)
          predicted_ids = tf.argmax(predictions, axis=-1).numpy()
          # prediction_complete_mask.shape = (batch_size)
          prediction_complete_mask = tf.math.logical_or(prediction_complete_mask, tf.math.equal(predicted_ids, end_index))
          
          # batch_attention_weights.shape = (batch_size, max_length_inp)
          batch_attention_weights = tf.reshape(batch_attention_weights, (batch_size, -1)).numpy()
          
          # decide on next word for each translation
          for i in range(batch_size):
            # continue if prediction for this sentence is complete
            if prediction_complete_mask[i].numpy():
              continue
            
            sequence = batch_sequences[i + offset]
            split = batch_splits[i + offset]
            attention_weights = batch_attention_weights[i][:len(sequence)]
            max_attention = np.argmax(attention_weights)
            
            if predicted_ids[i] == 1 and len(split) > 2:
              while split[max_attention] == '<start>' or split[max_attention] == '<end>':
                attention_weights[max_attention] = 0
                max_attention = np.argmax(attention_weights)
                
              if (1 in sequence):
                while sequence[max_attention] != 1:
                  attention_weights[max_attention] = 0
                  max_attention = np.argmax(attention_weights)
                  
              translations[i] += split[max_attention] + ' '
            else:
              translations[i] += self.targ_dict.index_word[predicted_ids[i]] + ' '
          
          dec_input = tf.expand_dims(predicted_ids, 1)
        
        # for last batch, drop the padded strings
        # if pad_amount is 0, the last batch would be dropped if we executed this
        if pad_amount and batch_index == (number_of_batches - 1):
          translations = translations[:-pad_amount]
        
        for i in range(len(translations)):
          output_file.write(translations[i])
          output_file.write("\n")
  
  
  # helper function to translate some test file using every available epoch, helps finding best epoch
  def check_epochs(self, model_dir, inp_file, batch_size=128):
    print("Exploring epochs")
    
    epoch = 1
    while(True):
      try:
        self.load_from_dir(model_dir, epoch=epoch)
        
        self.translate_to_file(inp_file, os.path.join(model_dir, inp_file + '_epoch{:02}'.format(epoch)), batch_size=batch_size)
        
        epoch += 1
      except:
        break


# various parameter parsing to control this script
parser = argparse.ArgumentParser()
parser.add_argument("-t", "--train", help="train model to directory", action="store_true")
parser.add_argument("-l", "--load", help="load model from directory", action="store_true")
parser.add_argument("-d", "--model_dir", help="directory to store/load model", default=".")
parser.add_argument("-i", "--input", help="input (input corpus for --train or input file for --load)")
parser.add_argument("-o", "--output", help="output (target corpus for --train or output file for --load)")
parser.add_argument("-b", "--batch_size", help="batch size for batch gradient descent", type=int, default=None)
parser.add_argument("-e", "--epochs", help="number of epochs to train", type=int, default=None)
parser.add_argument("-f", "--frequency", help="tokens with a frequency count lower than this value are replaced by '<UNK>'", type=int, default=0)
parser.add_argument("-m", "--max_length", help="maximum number of tokens per sentence (includes <start> and <end> tokens)", type=int, default=0)
parser.add_argument("-n", "--num_examples", help="number of sentences to use for training", type=int, default=None)
parser.add_argument("-p", "--dropout", help="dropout rate (float from interval [0,1))", type=float, default=0.2)
parser.add_argument("-c", "--cont", help="continue training from last checkpoint", action="store_true")
parser.add_argument("-u", "--cpu", help="use CPU instead of GPU", action="store_true")
parser.add_argument("-x", "--explore", help="translate file using every available checkpoint", action="store_true")
args = parser.parse_args()

if args.train == args.load:
  print("Error: Invalid mode selection")
  exit()

translator = Translator()

if args.cpu:
  # make GPU invisible to tensorflow through environment variable
  os.environ["CUDA_VISIBLE_DEVICES"]="-1"
  print("Using tensorflow on CPU")

if args.model_dir == ".":
  print("Warning: model_dir not specified, defaulting to '{}'".format(args.model_dir))

if(args.load):
  if args.input:
    if not args.batch_size:
      args.batch_size = 128
    
    if args.explore:
      translator.check_epochs(args.model_dir, args.input, args.batch_size)
    else:
      if not args.output:
        print("Error: Output file missing")
        exit()
      
      translator.load_from_dir(args.model_dir, epoch=args.epochs)
      translator.translate_to_file(args.input, args.output, args.batch_size)
  else:
    translator.load_from_dir(args.model_dir, epoch=args.epochs)
    translator.interactive()
else:
  print("Training model to directory '{}'".format(args.model_dir))
  
  if not args.input or not args.output:
    print("Error: Input or target corpus missing")
    exit()
  
  if(args.cont):
    print("Continuing from last checkpoint")
  print("Input corpus: {}".format(args.input))
  print("Target corpus: {}".format(args.output))
  if not args.batch_size:
    args.batch_size = 64
  print("Batch size: {}".format(args.batch_size))
  if not args.epochs:
    args.epochs = 10
  print("Number of epochs: {}".format(args.epochs))
  print("Minimum token frequency: {}".format(args.frequency))
  if args.max_length > 0:
    print("Maximum sentence length: {}".format(args.max_length))
  else:
    print("Maximum sentence length: None")
  if(args.num_examples):
    print("Training examples: {}".format(args.num_examples))
  print("Dropout rate: {}".format(args.dropout))
  
  translator.train_from_corpus( input_path=args.input,
                                target_path=args.output,
                                epochs=args.epochs,
                                batch_size=args.batch_size,
                                model_dir=args.model_dir,
                                min_freq=args.frequency,
                                max_length=args.max_length,
                                num_examples=args.num_examples,
                                dropout=args.dropout,
                                contin=args.cont)
